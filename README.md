## Final Project

## Kelompok 7

## Tentang Aplikasi

Aplikasi yang dibuat adalah aplikasi bimbingan belajar yang mengatur tentang jenjang pendidikan ( education level) para murid dan program yang ada di bimbel.

## Anggota Kelompok :

- Adrian Syah Abidin
- Ahmad Zaini
- Dicky Kurniawan Syahputra

## ERD

</br>

![ERD](finalproj.png)

</br>

## Link Video

- youtube : https://youtu.be/_CKJgaqFK7M
